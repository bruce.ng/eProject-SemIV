package com.minhnd.unit.test.location;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.Assert;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.test.TestCase;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserLocalServiceUtil;
//import com.liferay.portal.util.InitUtil;
import com.minhnd.model.Location;
import com.minhnd.service.LocationLocalService;
import com.minhnd.service.LocationLocalServiceUtil;

public class LocationUnitTest extends TestCase {
  private static Log log = LogFactoryUtil.getLog(LocationUnitTest.class);
  
  private LocationLocalService locationService;
  private UserLocalService userService;
  
  @Before
  protected void setUp() {
    try {
      System.out.println(">>>SET UP");
      //init the application context and beans for Spring.
//      InitUtil.initWithSpring();
      //Get the service for testing
      locationService = LocationLocalServiceUtil.getService();
      //Test LR services
      userService = UserLocalServiceUtil.getService();
      super.setUp();
    } catch (Exception ex) {
      ex.printStackTrace();
      fail("Init failed...");
    }
  }
  
  @After
  protected void tearDown() throws Exception {
    System.out.println(">>>TEAR DOWN");
    locationService = null;
    userService = null;
    super.tearDown();
  }
  
  public void testLocation() throws PortalException, SystemException {
    List<Location> locations = locationService.getLocations(-1, -1);
    Assert.assertNotNull(locations);
    log.info(locations.size());
  }
}
