/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link SpeakerLocalService}.
 * </p>
 *
 * @author    DTT-777
 * @see       SpeakerLocalService
 * @generated
 */
public class SpeakerLocalServiceWrapper implements SpeakerLocalService,
	ServiceWrapper<SpeakerLocalService> {
	public SpeakerLocalServiceWrapper(SpeakerLocalService speakerLocalService) {
		_speakerLocalService = speakerLocalService;
	}

	/**
	* Adds the speaker to the database. Also notifies the appropriate model listeners.
	*
	* @param speaker the speaker
	* @return the speaker that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker addSpeaker(com.minhnd.model.Speaker speaker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.addSpeaker(speaker);
	}

	/**
	* Creates a new speaker with the primary key. Does not add the speaker to the database.
	*
	* @param id the primary key for the new speaker
	* @return the new speaker
	*/
	public com.minhnd.model.Speaker createSpeaker(long id) {
		return _speakerLocalService.createSpeaker(id);
	}

	/**
	* Deletes the speaker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the speaker
	* @throws PortalException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deleteSpeaker(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_speakerLocalService.deleteSpeaker(id);
	}

	/**
	* Deletes the speaker from the database. Also notifies the appropriate model listeners.
	*
	* @param speaker the speaker
	* @throws SystemException if a system exception occurred
	*/
	public void deleteSpeaker(com.minhnd.model.Speaker speaker)
		throws com.liferay.portal.kernel.exception.SystemException {
		_speakerLocalService.deleteSpeaker(speaker);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.minhnd.model.Speaker fetchSpeaker(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.fetchSpeaker(id);
	}

	/**
	* Returns the speaker with the primary key.
	*
	* @param id the primary key of the speaker
	* @return the speaker
	* @throws PortalException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker getSpeaker(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.getSpeaker(id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the speakers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @return the range of speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> getSpeakers(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.getSpeakers(start, end);
	}

	/**
	* Returns the number of speakers.
	*
	* @return the number of speakers
	* @throws SystemException if a system exception occurred
	*/
	public int getSpeakersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.getSpeakersCount();
	}

	/**
	* Updates the speaker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param speaker the speaker
	* @return the speaker that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker updateSpeaker(
		com.minhnd.model.Speaker speaker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.updateSpeaker(speaker);
	}

	/**
	* Updates the speaker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param speaker the speaker
	* @param merge whether to merge the speaker with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the speaker that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker updateSpeaker(
		com.minhnd.model.Speaker speaker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _speakerLocalService.updateSpeaker(speaker, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _speakerLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_speakerLocalService.setBeanIdentifier(beanIdentifier);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public SpeakerLocalService getWrappedSpeakerLocalService() {
		return _speakerLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedSpeakerLocalService(
		SpeakerLocalService speakerLocalService) {
		_speakerLocalService = speakerLocalService;
	}

	public SpeakerLocalService getWrappedService() {
		return _speakerLocalService;
	}

	public void setWrappedService(SpeakerLocalService speakerLocalService) {
		_speakerLocalService = speakerLocalService;
	}

	private SpeakerLocalService _speakerLocalService;
}