/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.minhnd.model.Speaker;

import java.util.List;

/**
 * The persistence utility for the speaker service. This utility wraps {@link SpeakerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author DTT-777
 * @see SpeakerPersistence
 * @see SpeakerPersistenceImpl
 * @generated
 */
public class SpeakerUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Speaker speaker) {
		getPersistence().clearCache(speaker);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Speaker> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Speaker> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Speaker> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Speaker update(Speaker speaker, boolean merge)
		throws SystemException {
		return getPersistence().update(speaker, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Speaker update(Speaker speaker, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(speaker, merge, serviceContext);
	}

	/**
	* Caches the speaker in the entity cache if it is enabled.
	*
	* @param speaker the speaker
	*/
	public static void cacheResult(com.minhnd.model.Speaker speaker) {
		getPersistence().cacheResult(speaker);
	}

	/**
	* Caches the speakers in the entity cache if it is enabled.
	*
	* @param speakers the speakers
	*/
	public static void cacheResult(
		java.util.List<com.minhnd.model.Speaker> speakers) {
		getPersistence().cacheResult(speakers);
	}

	/**
	* Creates a new speaker with the primary key. Does not add the speaker to the database.
	*
	* @param id the primary key for the new speaker
	* @return the new speaker
	*/
	public static com.minhnd.model.Speaker create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the speaker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the speaker
	* @return the speaker that was removed
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException {
		return getPersistence().remove(id);
	}

	public static com.minhnd.model.Speaker updateImpl(
		com.minhnd.model.Speaker speaker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(speaker, merge);
	}

	/**
	* Returns the speaker with the primary key or throws a {@link com.minhnd.NoSuchSpeakerException} if it could not be found.
	*
	* @param id the primary key of the speaker
	* @return the speaker
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the speaker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the speaker
	* @return the speaker, or <code>null</code> if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the speakers where name = &#63;.
	*
	* @param name the name
	* @return the matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name);
	}

	/**
	* Returns a range of all the speakers where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @return the range of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end);
	}

	/**
	* Returns an ordered range of all the speakers where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByname(name, start, end, orderByComparator);
	}

	/**
	* Returns the first speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching speaker
	* @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker findByname_First(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException {
		return getPersistence().findByname_First(name, orderByComparator);
	}

	/**
	* Returns the last speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching speaker
	* @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker findByname_Last(
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException {
		return getPersistence().findByname_Last(name, orderByComparator);
	}

	/**
	* Returns the speakers before and after the current speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param id the primary key of the current speaker
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next speaker
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.minhnd.model.Speaker[] findByname_PrevAndNext(long id,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException {
		return getPersistence()
				   .findByname_PrevAndNext(id, name, orderByComparator);
	}

	/**
	* Returns all the speakers.
	*
	* @return the speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the speakers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @return the range of speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the speakers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of speakers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.minhnd.model.Speaker> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the speakers where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByname(name);
	}

	/**
	* Removes all the speakers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of speakers where name = &#63;.
	*
	* @param name the name
	* @return the number of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByname(name);
	}

	/**
	* Returns the number of speakers.
	*
	* @return the number of speakers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SpeakerPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SpeakerPersistence)PortletBeanLocatorUtil.locate(com.minhnd.service.ClpSerializer.getServletContextName(),
					SpeakerPersistence.class.getName());

			ReferenceRegistry.registerReference(SpeakerUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	public void setPersistence(SpeakerPersistence persistence) {
		_persistence = persistence;

		ReferenceRegistry.registerReference(SpeakerUtil.class, "_persistence");
	}

	private static SpeakerPersistence _persistence;
}