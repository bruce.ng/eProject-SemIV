/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    DTT-777
 * @generated
 */
public class SpeakerSoap implements Serializable {
	public static SpeakerSoap toSoapModel(Speaker model) {
		SpeakerSoap soapModel = new SpeakerSoap();

		soapModel.setId(model.getId());
		soapModel.setName(model.getName());
		soapModel.setImage(model.getImage());
		soapModel.setDescription(model.getDescription());

		return soapModel;
	}

	public static SpeakerSoap[] toSoapModels(Speaker[] models) {
		SpeakerSoap[] soapModels = new SpeakerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SpeakerSoap[][] toSoapModels(Speaker[][] models) {
		SpeakerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SpeakerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SpeakerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SpeakerSoap[] toSoapModels(List<Speaker> models) {
		List<SpeakerSoap> soapModels = new ArrayList<SpeakerSoap>(models.size());

		for (Speaker model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SpeakerSoap[soapModels.size()]);
	}

	public SpeakerSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	private long _id;
	private String _name;
	private String _image;
	private String _description;
}